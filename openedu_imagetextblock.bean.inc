<?php
/**
 * @file
 * openedu_imagetextblock.bean.inc
 */

/**
 * Implements hook_bean_admin_ui_types().
 */
function openedu_imagetextblock_bean_admin_ui_types() {
  $export = array();

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'image_text_block';
  $bean_type->label = 'Image and Text Block';
  $bean_type->options = '';
  $bean_type->description = 'This is a generic Image and Text block that can be placed throughout a site for multiple use cases.';
  $export['image_text_block'] = $bean_type;

  return $export;
}
